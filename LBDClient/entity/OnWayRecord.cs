﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class OnWayRecord
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int shipRecordId;

        public int ShipRecordId
        {
            get { return shipRecordId; }
            set { shipRecordId = value; }
        }
        private string stationName;

        public string StationName
        {
            get { return stationName; }
            set { stationName = value; }
        }
        private DateTime arriveTime;

        public DateTime ArriveTime
        {
            get { return arriveTime; }
            set { arriveTime = value; }
        }
        private DateTime departTime;

        public DateTime DepartTime
        {
            get { return departTime; }
            set { departTime = value; }
        }
    }
}
