﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class ShipperInfo
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
