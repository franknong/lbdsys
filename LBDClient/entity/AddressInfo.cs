﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class AddressInfo
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string baseAddr;

        public string BaseAddr
        {
            get { return baseAddr; }
            set { baseAddr = value; }
        }
        private string detailAddr;

        public string DetailAddr
        {
            get { return detailAddr; }
            set { detailAddr = value; }
        }
        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
