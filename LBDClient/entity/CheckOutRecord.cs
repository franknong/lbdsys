﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class CheckOutRecord
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int shipItemDetailId;

        public int ShipItemDetailId
        {
            get { return shipItemDetailId; }
            set { shipItemDetailId = value; }
        }
        private DateTime checkoutTime;

        public DateTime CheckoutTime
        {
            get { return checkoutTime; }
            set { checkoutTime = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private int checkOutNo;

        public int CheckOutNo
        {
            get { return checkOutNo; }
            set { checkOutNo = value; }
        }
    }
}
