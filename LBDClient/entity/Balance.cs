﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class Balance
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private DateTime time;

        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
        private string type;

        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        private double value;

        public double Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
        private int operatorId;

        public int OperatorId
        {
            get { return operatorId; }
            set { operatorId = value; }
        }
        private string who;

        public string Who
        {
            get { return who; }
            set { who = value; }
        }
        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
