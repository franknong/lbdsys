﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class CarInfo
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string cardNo;

        public string CardNo
        {
            get { return cardNo; }
            set { cardNo = value; }
        }
        private string owner;

        public string Owner
        {
            get { return owner; }
            set { owner = value; }
        }
        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        private DateTime buyDate;

        public DateTime BuyDate
        {
            get { return buyDate; }
            set { buyDate = value; }
        }
        private string typeName;

        public string TypeName
        {
            get { return typeName; }
            set { typeName = value; }
        }
        private int volume;

        public int Volume
        {
            get { return volume; }
            set { volume = value; }
        }
        private int ton;

        public int Ton
        {
            get { return ton; }
            set { ton = value; }
        }
        private DateTime auditDate;

        public DateTime AuditDate
        {
            get { return auditDate; }
            set { auditDate = value; }
        }
        private DateTime latestMainten;

        public DateTime LatestMainten
        {
            get { return latestMainten; }
            set { latestMainten = value; }
        }
        private double costPerShip;

        public double CostPerShip
        {
            get { return costPerShip; }
            set { costPerShip = value; }
        }
        private string airFilterType;

        public string AirFilterType
        {
            get { return airFilterType; }
            set { airFilterType = value; }
        }
        private string dieselFilterType;

        public string DieselFilterType
        {
            get { return dieselFilterType; }
            set { dieselFilterType = value; }
        }
        private string lubricantFilterType;

        public string LubricantFilterType
        {
            get { return lubricantFilterType; }
            set { lubricantFilterType = value; }
        }
        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
