﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class CheckInRecord
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int employeeId;

        public int EmployeeId
        {
            get { return employeeId; }
            set { employeeId = value; }
        }
        private DateTime checkInTime;

        public DateTime CheckInTime
        {
            get { return checkInTime; }
            set { checkInTime = value; }
        }
        private DateTime checkOutTime;

        public DateTime CheckOutTime
        {
            get { return checkOutTime; }
            set { checkOutTime = value; }
        }
        private string workplace;

        public string Workplace
        {
            get { return workplace; }
            set { workplace = value; }
        }
        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
