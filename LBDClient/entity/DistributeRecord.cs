﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class DistributeRecord
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int onWayRecordId;

        public int OnWayRecordId
        {
            get { return onWayRecordId; }
            set { onWayRecordId = value; }
        }
        private int employeeId;

        public int EmployeeId
        {
            get { return employeeId; }
            set { employeeId = value; }
        }
    }
}
