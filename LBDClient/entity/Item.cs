﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class Item
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string classInfo;

        public string ClassInfo
        {
            get { return classInfo; }
            set { classInfo = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string package;

        public string Package
        {
            get { return package; }
            set { package = value; }
        }
        private int length;

        public int Length
        {
            get { return length; }
            set { length = value; }
        }
        private int width;

        public int Width
        {
            get { return width; }
            set { width = value; }
        }
        private int height;

        public int Height
        {
            get { return height; }
            set { height = value; }
        }
        private float weight;

        public float Weight
        {
            get { return weight; }
            set { weight = value; }
        }
        private double value;

        public double Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
        private string conveyAttr;

        public string ConveyAttr
        {
            get { return conveyAttr; }
            set { conveyAttr = value; }
        }
        private double recommendPrice;

        public double RecommendPrice
        {
            get { return recommendPrice; }
            set { recommendPrice = value; }
        }
        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
