﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class ShipItemDetails
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int shipListId;

        public int ShipListId
        {
            get { return shipListId; }
            set { shipListId = value; }
        }
        private int itemId;

        public int ItemId
        {
            get { return itemId; }
            set { itemId = value; }
        }
        private int itemNumber;

        public int ItemNumber
        {
            get { return itemNumber; }
            set { itemNumber = value; }
        }
        private DateTime receiveTime;

        public DateTime ReceiveTime
        {
            get { return receiveTime; }
            set { receiveTime = value; }
        }
        private double delegateFee;

        public double DelegateFee
        {
            get { return delegateFee; }
            set { delegateFee = value; }
        }
        private double insurance;

        public double Insurance
        {
            get { return insurance; }
            set { insurance = value; }
        }
        private double setPrice;

        public double SetPrice
        {
            get { return setPrice; }
            set { setPrice = value; }
        }
        private string payType;

        public string PayType
        {
            get { return payType; }
            set { payType = value; }
        }
        private double havePay;

        public double HavePay
        {
            get { return havePay; }
            set { havePay = value; }
        }
        private double refund;

        public double Refund
        {
            get { return refund; }
            set { refund = value; }
        }
        private string finsihStatus;

        public string FinsihStatus
        {
            get { return finsihStatus; }
            set { finsihStatus = value; }
        }
    }
}
