﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class ConsigneeInfo
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        private int addressId;

        public int AddressId
        {
            get { return addressId; }
            set { addressId = value; }
        }
        private string bakPhone;

        public string BakPhone
        {
            get { return bakPhone; }
            set { bakPhone = value; }
        }
        private string qq;

        public string Qq
        {
            get { return qq; }
            set { qq = value; }
        }
        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        private string wechat;

        public string Wechat
        {
            get { return wechat; }
            set { wechat = value; }
        }
        private int point;

        public int Point
        {
            get { return point; }
            set { point = value; }
        }
        private string bankName;

        public string BankName
        {
            get { return bankName; }
            set { bankName = value; }
        }
        private string bankCardOwner;

        public string BankCardOwner
        {
            get { return bankCardOwner; }
            set { bankCardOwner = value; }
        }
        private string bankCardNo;

        public string BankCardNo
        {
            get { return bankCardNo; }
            set { bankCardNo = value; }
        }
        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
