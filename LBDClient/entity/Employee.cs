﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class Employee
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string sex;

        public string Sex
        {
            get { return sex; }
            set { sex = value; }
        }
        private DateTime birthday;

        public DateTime Birthday
        {
            get { return birthday; }
            set { birthday = value; }
        }
        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        private string bakPhone;

        public string BakPhone
        {
            get { return bakPhone; }
            set { bakPhone = value; }
        }
        private string idCardNo;

        public string IdCardNo
        {
            get { return idCardNo; }
            set { idCardNo = value; }
        }
        private float salary;

        public float Salary
        {
            get { return salary; }
            set { salary = value; }
        }
        private DateTime enrolldate;

        public DateTime Enrolldate
        {
            get { return enrolldate; }
            set { enrolldate = value; }
        }
        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
