﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class ShipRecord
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int carId;

        public int CarId
        {
            get { return carId; }
            set { carId = value; }
        }
        private int driverId;

        public int DriverId
        {
            get { return driverId; }
            set { driverId = value; }
        }
        private int bakDriverId;

        public int BakDriverId
        {
            get { return bakDriverId; }
            set { bakDriverId = value; }
        }
        private int distributeRecordId;

        public int DistributeRecordId
        {
            get { return distributeRecordId; }
            set { distributeRecordId = value; }
        }
        private string fromStation;

        public string FromStation
        {
            get { return fromStation; }
            set { fromStation = value; }
        }
        private string termialStation;

        public string TermialStation
        {
            get { return termialStation; }
            set { termialStation = value; }
        }
        private DateTime loadTime;

        public DateTime LoadTime
        {
            get { return loadTime; }
            set { loadTime = value; }
        }
        private DateTime planDepartTime;

        public DateTime PlanDepartTime
        {
            get { return planDepartTime; }
            set { planDepartTime = value; }
        }
        private DateTime planArriveTime;

        public DateTime PlanArriveTime
        {
            get { return planArriveTime; }
            set { planArriveTime = value; }
        }
        private DateTime factDepartTime;

        public DateTime FactDepartTime
        {
            get { return factDepartTime; }
            set { factDepartTime = value; }
        }
        private DateTime factArriveTime;

        public DateTime FactArriveTime
        {
            get { return factArriveTime; }
            set { factArriveTime = value; }
        }
    }
}
