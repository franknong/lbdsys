﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class TempEmployee
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string phone;

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        private string role;

        public string Role
        {
            get { return role; }
            set { role = value; }
        }
        private string sex;

        public string Sex
        {
            get { return sex; }
            set { sex = value; }
        }
        private string idCardNo;

        public string IdCardNo
        {
            get { return idCardNo; }
            set { idCardNo = value; }
        }
        private double costPerHour;

        public double CostPerHour
        {
            get { return costPerHour; }
            set { costPerHour = value; }
        }
        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }
    }
}
