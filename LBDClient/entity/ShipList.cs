﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.entity
{
    class ShipList
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string listNo;

        public string ListNo
        {
            get { return listNo; }
            set { listNo = value; }
        }
        private int shipperId;

        public int ShipperId
        {
            get { return shipperId; }
            set { shipperId = value; }
        }
        private int consigneeId;

        public int ConsigneeId
        {
            get { return consigneeId; }
            set { consigneeId = value; }
        }
        private int shipRecordId;

        public int ShipRecordId
        {
            get { return shipRecordId; }
            set { shipRecordId = value; }
        }
    }
}
