﻿namespace LBDClient
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tcCheckIn = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.cbItem5Class = new System.Windows.Forms.ComboBox();
            this.nuUDItem5Cnt = new System.Windows.Forms.NumericUpDown();
            this.cbItem5Name = new System.Windows.Forms.ComboBox();
            this.tbItem5Freight = new System.Windows.Forms.TextBox();
            this.cbItem5Pack = new System.Windows.Forms.ComboBox();
            this.tbItem5Price = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.tbItem5Wt = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.tbItem5Vol = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cbItem4Class = new System.Windows.Forms.ComboBox();
            this.nuUDItem4Cnt = new System.Windows.Forms.NumericUpDown();
            this.cbItem4Name = new System.Windows.Forms.ComboBox();
            this.tbItem4Freight = new System.Windows.Forms.TextBox();
            this.cbItem4Pack = new System.Windows.Forms.ComboBox();
            this.tbItem4Price = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.tbItem4Wt = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tbItem4Vol = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbItem3Class = new System.Windows.Forms.ComboBox();
            this.nuUDItem3Cnt = new System.Windows.Forms.NumericUpDown();
            this.cbItem3Name = new System.Windows.Forms.ComboBox();
            this.tbItem3Freight = new System.Windows.Forms.TextBox();
            this.cbItem3Pack = new System.Windows.Forms.ComboBox();
            this.tbItem3Price = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.tbItem3Wt = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tbItem3Vol = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbItem2Class = new System.Windows.Forms.ComboBox();
            this.nuUDItem2Cnt = new System.Windows.Forms.NumericUpDown();
            this.cbItem2Name = new System.Windows.Forms.ComboBox();
            this.tbItem2Freight = new System.Windows.Forms.TextBox();
            this.cbItem2Pack = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.tbItem2Price = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbItem2Wt = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbItem2Vol = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbItem1Class = new System.Windows.Forms.ComboBox();
            this.nuUDItem1Cnt = new System.Windows.Forms.NumericUpDown();
            this.cbItem1Name = new System.Windows.Forms.ComboBox();
            this.tbItem1Freight = new System.Windows.Forms.TextBox();
            this.cbItem1Pack = new System.Windows.Forms.ComboBox();
            this.tbItem1Price = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbItem1Wt = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbItem1Vol = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.tcCheckIn.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuUDItem5Cnt)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuUDItem4Cnt)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuUDItem3Cnt)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuUDItem2Cnt)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuUDItem1Cnt)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcCheckIn
            // 
            this.tcCheckIn.Controls.Add(this.tabPage1);
            this.tcCheckIn.Controls.Add(this.tabPage2);
            this.tcCheckIn.Controls.Add(this.tabPage3);
            this.tcCheckIn.Controls.Add(this.tabPage4);
            this.tcCheckIn.Controls.Add(this.tabPage5);
            this.tcCheckIn.Controls.Add(this.tabPage6);
            this.tcCheckIn.Location = new System.Drawing.Point(2, 1);
            this.tcCheckIn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tcCheckIn.Name = "tcCheckIn";
            this.tcCheckIn.SelectedIndex = 0;
            this.tcCheckIn.Size = new System.Drawing.Size(944, 538);
            this.tcCheckIn.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage1.Size = new System.Drawing.Size(936, 512);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "托运开单";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.panel5);
            this.groupBox4.Controls.Add(this.panel4);
            this.groupBox4.Controls.Add(this.panel3);
            this.groupBox4.Controls.Add(this.panel2);
            this.groupBox4.Controls.Add(this.panel1);
            this.groupBox4.ForeColor = System.Drawing.Color.Navy;
            this.groupBox4.Location = new System.Drawing.Point(14, 215);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Size = new System.Drawing.Size(914, 220);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "物品信息";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.cbItem5Class);
            this.panel5.Controls.Add(this.nuUDItem5Cnt);
            this.panel5.Controls.Add(this.cbItem5Name);
            this.panel5.Controls.Add(this.tbItem5Freight);
            this.panel5.Controls.Add(this.cbItem5Pack);
            this.panel5.Controls.Add(this.tbItem5Price);
            this.panel5.Controls.Add(this.label53);
            this.panel5.Controls.Add(this.label46);
            this.panel5.Controls.Add(this.tbItem5Wt);
            this.panel5.Controls.Add(this.label47);
            this.panel5.Controls.Add(this.tbItem5Vol);
            this.panel5.Controls.Add(this.label48);
            this.panel5.Controls.Add(this.label49);
            this.panel5.Controls.Add(this.label50);
            this.panel5.Controls.Add(this.label51);
            this.panel5.Controls.Add(this.label52);
            this.panel5.Enabled = false;
            this.panel5.Location = new System.Drawing.Point(4, 181);
            this.panel5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(905, 34);
            this.panel5.TabIndex = 4;
            // 
            // cbItem5Class
            // 
            this.cbItem5Class.FormattingEnabled = true;
            this.cbItem5Class.Items.AddRange(new object[] {
            "食品",
            "五金",
            "冻品",
            "建材",
            "农机",
            "日杂"});
            this.cbItem5Class.Location = new System.Drawing.Point(48, 6);
            this.cbItem5Class.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem5Class.Name = "cbItem5Class";
            this.cbItem5Class.Size = new System.Drawing.Size(55, 25);
            this.cbItem5Class.TabIndex = 0;
            // 
            // nuUDItem5Cnt
            // 
            this.nuUDItem5Cnt.Location = new System.Drawing.Point(720, 6);
            this.nuUDItem5Cnt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nuUDItem5Cnt.Name = "nuUDItem5Cnt";
            this.nuUDItem5Cnt.Size = new System.Drawing.Size(54, 23);
            this.nuUDItem5Cnt.TabIndex = 3;
            // 
            // cbItem5Name
            // 
            this.cbItem5Name.FormattingEnabled = true;
            this.cbItem5Name.Location = new System.Drawing.Point(152, 6);
            this.cbItem5Name.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem5Name.Name = "cbItem5Name";
            this.cbItem5Name.Size = new System.Drawing.Size(89, 25);
            this.cbItem5Name.TabIndex = 0;
            // 
            // tbItem5Freight
            // 
            this.tbItem5Freight.Location = new System.Drawing.Point(832, 6);
            this.tbItem5Freight.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem5Freight.Name = "tbItem5Freight";
            this.tbItem5Freight.Size = new System.Drawing.Size(64, 23);
            this.tbItem5Freight.TabIndex = 2;
            // 
            // cbItem5Pack
            // 
            this.cbItem5Pack.FormattingEnabled = true;
            this.cbItem5Pack.Items.AddRange(new object[] {
            "纸箱",
            "编织袋",
            "泡棉",
            "气泡袋",
            "其他",
            "无"});
            this.cbItem5Pack.Location = new System.Drawing.Point(300, 6);
            this.cbItem5Pack.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem5Pack.Name = "cbItem5Pack";
            this.cbItem5Pack.Size = new System.Drawing.Size(55, 25);
            this.cbItem5Pack.TabIndex = 0;
            // 
            // tbItem5Price
            // 
            this.tbItem5Price.Location = new System.Drawing.Point(610, 6);
            this.tbItem5Price.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem5Price.Name = "tbItem5Price";
            this.tbItem5Price.Size = new System.Drawing.Size(57, 23);
            this.tbItem5Price.TabIndex = 2;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(678, 8);
            this.label53.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(44, 17);
            this.label53.TabIndex = 1;
            this.label53.Text = "件数：";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 9);
            this.label46.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(44, 17);
            this.label46.TabIndex = 1;
            this.label46.Text = "类别：";
            // 
            // tbItem5Wt
            // 
            this.tbItem5Wt.Location = new System.Drawing.Point(508, 6);
            this.tbItem5Wt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem5Wt.Name = "tbItem5Wt";
            this.tbItem5Wt.Size = new System.Drawing.Size(55, 23);
            this.tbItem5Wt.TabIndex = 2;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(111, 9);
            this.label47.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(44, 17);
            this.label47.TabIndex = 1;
            this.label47.Text = "品名：";
            // 
            // tbItem5Vol
            // 
            this.tbItem5Vol.Location = new System.Drawing.Point(404, 6);
            this.tbItem5Vol.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem5Vol.Name = "tbItem5Vol";
            this.tbItem5Vol.Size = new System.Drawing.Size(55, 23);
            this.tbItem5Vol.TabIndex = 2;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(256, 9);
            this.label48.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(44, 17);
            this.label48.TabIndex = 1;
            this.label48.Text = "包装：";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(791, 8);
            this.label49.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(44, 17);
            this.label49.TabIndex = 1;
            this.label49.Text = "运费：";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(362, 9);
            this.label50.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(44, 17);
            this.label50.TabIndex = 1;
            this.label50.Text = "体积：";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(570, 9);
            this.label51.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(44, 17);
            this.label51.TabIndex = 1;
            this.label51.Text = "单价：";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(466, 9);
            this.label52.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(44, 17);
            this.label52.TabIndex = 1;
            this.label52.Text = "重量：";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cbItem4Class);
            this.panel4.Controls.Add(this.nuUDItem4Cnt);
            this.panel4.Controls.Add(this.cbItem4Name);
            this.panel4.Controls.Add(this.tbItem4Freight);
            this.panel4.Controls.Add(this.cbItem4Pack);
            this.panel4.Controls.Add(this.tbItem4Price);
            this.panel4.Controls.Add(this.label45);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.tbItem4Wt);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.tbItem4Vol);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.label41);
            this.panel4.Controls.Add(this.label42);
            this.panel4.Controls.Add(this.label43);
            this.panel4.Controls.Add(this.label44);
            this.panel4.Enabled = false;
            this.panel4.Location = new System.Drawing.Point(4, 141);
            this.panel4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(905, 34);
            this.panel4.TabIndex = 4;
            // 
            // cbItem4Class
            // 
            this.cbItem4Class.FormattingEnabled = true;
            this.cbItem4Class.Items.AddRange(new object[] {
            "食品",
            "五金",
            "冻品",
            "建材",
            "农机",
            "日杂"});
            this.cbItem4Class.Location = new System.Drawing.Point(48, 6);
            this.cbItem4Class.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem4Class.Name = "cbItem4Class";
            this.cbItem4Class.Size = new System.Drawing.Size(55, 25);
            this.cbItem4Class.TabIndex = 0;
            // 
            // nuUDItem4Cnt
            // 
            this.nuUDItem4Cnt.Location = new System.Drawing.Point(720, 10);
            this.nuUDItem4Cnt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nuUDItem4Cnt.Name = "nuUDItem4Cnt";
            this.nuUDItem4Cnt.Size = new System.Drawing.Size(54, 23);
            this.nuUDItem4Cnt.TabIndex = 3;
            // 
            // cbItem4Name
            // 
            this.cbItem4Name.FormattingEnabled = true;
            this.cbItem4Name.Location = new System.Drawing.Point(152, 6);
            this.cbItem4Name.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem4Name.Name = "cbItem4Name";
            this.cbItem4Name.Size = new System.Drawing.Size(89, 25);
            this.cbItem4Name.TabIndex = 0;
            // 
            // tbItem4Freight
            // 
            this.tbItem4Freight.Location = new System.Drawing.Point(832, 6);
            this.tbItem4Freight.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem4Freight.Name = "tbItem4Freight";
            this.tbItem4Freight.Size = new System.Drawing.Size(64, 23);
            this.tbItem4Freight.TabIndex = 2;
            // 
            // cbItem4Pack
            // 
            this.cbItem4Pack.FormattingEnabled = true;
            this.cbItem4Pack.Items.AddRange(new object[] {
            "纸箱",
            "编织袋",
            "泡棉",
            "气泡袋",
            "其他",
            "无"});
            this.cbItem4Pack.Location = new System.Drawing.Point(300, 6);
            this.cbItem4Pack.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem4Pack.Name = "cbItem4Pack";
            this.cbItem4Pack.Size = new System.Drawing.Size(55, 25);
            this.cbItem4Pack.TabIndex = 0;
            // 
            // tbItem4Price
            // 
            this.tbItem4Price.Location = new System.Drawing.Point(610, 6);
            this.tbItem4Price.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem4Price.Name = "tbItem4Price";
            this.tbItem4Price.Size = new System.Drawing.Size(57, 23);
            this.tbItem4Price.TabIndex = 2;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(678, 13);
            this.label45.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(44, 17);
            this.label45.TabIndex = 1;
            this.label45.Text = "件数：";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 9);
            this.label38.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(44, 17);
            this.label38.TabIndex = 1;
            this.label38.Text = "类别：";
            // 
            // tbItem4Wt
            // 
            this.tbItem4Wt.Location = new System.Drawing.Point(508, 6);
            this.tbItem4Wt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem4Wt.Name = "tbItem4Wt";
            this.tbItem4Wt.Size = new System.Drawing.Size(55, 23);
            this.tbItem4Wt.TabIndex = 2;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(111, 9);
            this.label39.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(44, 17);
            this.label39.TabIndex = 1;
            this.label39.Text = "品名：";
            // 
            // tbItem4Vol
            // 
            this.tbItem4Vol.Location = new System.Drawing.Point(404, 6);
            this.tbItem4Vol.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem4Vol.Name = "tbItem4Vol";
            this.tbItem4Vol.Size = new System.Drawing.Size(55, 23);
            this.tbItem4Vol.TabIndex = 2;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(256, 9);
            this.label40.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(44, 17);
            this.label40.TabIndex = 1;
            this.label40.Text = "包装：";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(791, 8);
            this.label41.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(44, 17);
            this.label41.TabIndex = 1;
            this.label41.Text = "运费：";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(362, 9);
            this.label42.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(44, 17);
            this.label42.TabIndex = 1;
            this.label42.Text = "体积：";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(570, 9);
            this.label43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(44, 17);
            this.label43.TabIndex = 1;
            this.label43.Text = "单价：";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(466, 9);
            this.label44.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(44, 17);
            this.label44.TabIndex = 1;
            this.label44.Text = "重量：";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cbItem3Class);
            this.panel3.Controls.Add(this.nuUDItem3Cnt);
            this.panel3.Controls.Add(this.cbItem3Name);
            this.panel3.Controls.Add(this.tbItem3Freight);
            this.panel3.Controls.Add(this.cbItem3Pack);
            this.panel3.Controls.Add(this.tbItem3Price);
            this.panel3.Controls.Add(this.label37);
            this.panel3.Controls.Add(this.label30);
            this.panel3.Controls.Add(this.tbItem3Wt);
            this.panel3.Controls.Add(this.label31);
            this.panel3.Controls.Add(this.tbItem3Vol);
            this.panel3.Controls.Add(this.label32);
            this.panel3.Controls.Add(this.label33);
            this.panel3.Controls.Add(this.label34);
            this.panel3.Controls.Add(this.label35);
            this.panel3.Controls.Add(this.label36);
            this.panel3.Enabled = false;
            this.panel3.Location = new System.Drawing.Point(4, 101);
            this.panel3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(905, 34);
            this.panel3.TabIndex = 4;
            // 
            // cbItem3Class
            // 
            this.cbItem3Class.FormattingEnabled = true;
            this.cbItem3Class.Items.AddRange(new object[] {
            "食品",
            "五金",
            "冻品",
            "建材",
            "农机",
            "日杂"});
            this.cbItem3Class.Location = new System.Drawing.Point(48, 6);
            this.cbItem3Class.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem3Class.Name = "cbItem3Class";
            this.cbItem3Class.Size = new System.Drawing.Size(55, 25);
            this.cbItem3Class.TabIndex = 0;
            // 
            // nuUDItem3Cnt
            // 
            this.nuUDItem3Cnt.Location = new System.Drawing.Point(720, 6);
            this.nuUDItem3Cnt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nuUDItem3Cnt.Name = "nuUDItem3Cnt";
            this.nuUDItem3Cnt.Size = new System.Drawing.Size(54, 23);
            this.nuUDItem3Cnt.TabIndex = 3;
            // 
            // cbItem3Name
            // 
            this.cbItem3Name.FormattingEnabled = true;
            this.cbItem3Name.Location = new System.Drawing.Point(152, 6);
            this.cbItem3Name.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem3Name.Name = "cbItem3Name";
            this.cbItem3Name.Size = new System.Drawing.Size(89, 25);
            this.cbItem3Name.TabIndex = 0;
            // 
            // tbItem3Freight
            // 
            this.tbItem3Freight.Location = new System.Drawing.Point(832, 6);
            this.tbItem3Freight.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem3Freight.Name = "tbItem3Freight";
            this.tbItem3Freight.Size = new System.Drawing.Size(64, 23);
            this.tbItem3Freight.TabIndex = 2;
            // 
            // cbItem3Pack
            // 
            this.cbItem3Pack.FormattingEnabled = true;
            this.cbItem3Pack.Items.AddRange(new object[] {
            "纸箱",
            "编织袋",
            "泡棉",
            "气泡袋",
            "其他",
            "无"});
            this.cbItem3Pack.Location = new System.Drawing.Point(300, 6);
            this.cbItem3Pack.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem3Pack.Name = "cbItem3Pack";
            this.cbItem3Pack.Size = new System.Drawing.Size(55, 25);
            this.cbItem3Pack.TabIndex = 0;
            // 
            // tbItem3Price
            // 
            this.tbItem3Price.Location = new System.Drawing.Point(610, 6);
            this.tbItem3Price.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem3Price.Name = "tbItem3Price";
            this.tbItem3Price.Size = new System.Drawing.Size(57, 23);
            this.tbItem3Price.TabIndex = 2;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(678, 9);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(44, 17);
            this.label37.TabIndex = 1;
            this.label37.Text = "件数：";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 9);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(44, 17);
            this.label30.TabIndex = 1;
            this.label30.Text = "类别：";
            // 
            // tbItem3Wt
            // 
            this.tbItem3Wt.Location = new System.Drawing.Point(508, 6);
            this.tbItem3Wt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem3Wt.Name = "tbItem3Wt";
            this.tbItem3Wt.Size = new System.Drawing.Size(55, 23);
            this.tbItem3Wt.TabIndex = 2;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(111, 9);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(44, 17);
            this.label31.TabIndex = 1;
            this.label31.Text = "品名：";
            // 
            // tbItem3Vol
            // 
            this.tbItem3Vol.Location = new System.Drawing.Point(404, 6);
            this.tbItem3Vol.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem3Vol.Name = "tbItem3Vol";
            this.tbItem3Vol.Size = new System.Drawing.Size(55, 23);
            this.tbItem3Vol.TabIndex = 2;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(256, 9);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(44, 17);
            this.label32.TabIndex = 1;
            this.label32.Text = "包装：";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(791, 8);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(44, 17);
            this.label33.TabIndex = 1;
            this.label33.Text = "运费：";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(362, 9);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(44, 17);
            this.label34.TabIndex = 1;
            this.label34.Text = "体积：";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(570, 8);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(44, 17);
            this.label35.TabIndex = 1;
            this.label35.Text = "单价：";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(466, 9);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(44, 17);
            this.label36.TabIndex = 1;
            this.label36.Text = "重量：";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cbItem2Class);
            this.panel2.Controls.Add(this.nuUDItem2Cnt);
            this.panel2.Controls.Add(this.cbItem2Name);
            this.panel2.Controls.Add(this.tbItem2Freight);
            this.panel2.Controls.Add(this.cbItem2Pack);
            this.panel2.Controls.Add(this.label29);
            this.panel2.Controls.Add(this.tbItem2Price);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.tbItem2Wt);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.tbItem2Vol);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.label27);
            this.panel2.Controls.Add(this.label28);
            this.panel2.Enabled = false;
            this.panel2.Location = new System.Drawing.Point(4, 61);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(905, 34);
            this.panel2.TabIndex = 4;
            // 
            // cbItem2Class
            // 
            this.cbItem2Class.FormattingEnabled = true;
            this.cbItem2Class.Items.AddRange(new object[] {
            "食品",
            "五金",
            "冻品",
            "建材",
            "农机",
            "日杂"});
            this.cbItem2Class.Location = new System.Drawing.Point(48, 6);
            this.cbItem2Class.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem2Class.Name = "cbItem2Class";
            this.cbItem2Class.Size = new System.Drawing.Size(55, 25);
            this.cbItem2Class.TabIndex = 0;
            // 
            // nuUDItem2Cnt
            // 
            this.nuUDItem2Cnt.Location = new System.Drawing.Point(720, 7);
            this.nuUDItem2Cnt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nuUDItem2Cnt.Name = "nuUDItem2Cnt";
            this.nuUDItem2Cnt.Size = new System.Drawing.Size(54, 23);
            this.nuUDItem2Cnt.TabIndex = 3;
            // 
            // cbItem2Name
            // 
            this.cbItem2Name.FormattingEnabled = true;
            this.cbItem2Name.Location = new System.Drawing.Point(152, 6);
            this.cbItem2Name.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem2Name.Name = "cbItem2Name";
            this.cbItem2Name.Size = new System.Drawing.Size(89, 25);
            this.cbItem2Name.TabIndex = 0;
            // 
            // tbItem2Freight
            // 
            this.tbItem2Freight.Location = new System.Drawing.Point(832, 6);
            this.tbItem2Freight.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem2Freight.Name = "tbItem2Freight";
            this.tbItem2Freight.Size = new System.Drawing.Size(64, 23);
            this.tbItem2Freight.TabIndex = 2;
            // 
            // cbItem2Pack
            // 
            this.cbItem2Pack.FormattingEnabled = true;
            this.cbItem2Pack.Items.AddRange(new object[] {
            "纸箱",
            "编织袋",
            "泡棉",
            "气泡袋",
            "其他",
            "无"});
            this.cbItem2Pack.Location = new System.Drawing.Point(300, 6);
            this.cbItem2Pack.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem2Pack.Name = "cbItem2Pack";
            this.cbItem2Pack.Size = new System.Drawing.Size(55, 25);
            this.cbItem2Pack.TabIndex = 0;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(678, 10);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(44, 17);
            this.label29.TabIndex = 1;
            this.label29.Text = "件数：";
            // 
            // tbItem2Price
            // 
            this.tbItem2Price.Location = new System.Drawing.Point(610, 6);
            this.tbItem2Price.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem2Price.Name = "tbItem2Price";
            this.tbItem2Price.Size = new System.Drawing.Size(57, 23);
            this.tbItem2Price.TabIndex = 2;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 9);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 17);
            this.label22.TabIndex = 1;
            this.label22.Text = "类别：";
            // 
            // tbItem2Wt
            // 
            this.tbItem2Wt.Location = new System.Drawing.Point(508, 6);
            this.tbItem2Wt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem2Wt.Name = "tbItem2Wt";
            this.tbItem2Wt.Size = new System.Drawing.Size(55, 23);
            this.tbItem2Wt.TabIndex = 2;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(111, 9);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 17);
            this.label23.TabIndex = 1;
            this.label23.Text = "品名：";
            // 
            // tbItem2Vol
            // 
            this.tbItem2Vol.Location = new System.Drawing.Point(404, 6);
            this.tbItem2Vol.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem2Vol.Name = "tbItem2Vol";
            this.tbItem2Vol.Size = new System.Drawing.Size(55, 23);
            this.tbItem2Vol.TabIndex = 2;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(256, 9);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(44, 17);
            this.label24.TabIndex = 1;
            this.label24.Text = "包装：";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(791, 8);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(44, 17);
            this.label25.TabIndex = 1;
            this.label25.Text = "运费：";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(362, 9);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(44, 17);
            this.label26.TabIndex = 1;
            this.label26.Text = "体积：";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(570, 9);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(44, 17);
            this.label27.TabIndex = 1;
            this.label27.Text = "单价：";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(466, 9);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(44, 17);
            this.label28.TabIndex = 1;
            this.label28.Text = "重量：";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbItem1Class);
            this.panel1.Controls.Add(this.nuUDItem1Cnt);
            this.panel1.Controls.Add(this.cbItem1Name);
            this.panel1.Controls.Add(this.tbItem1Freight);
            this.panel1.Controls.Add(this.cbItem1Pack);
            this.panel1.Controls.Add(this.tbItem1Price);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.tbItem1Wt);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.tbItem1Vol);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Location = new System.Drawing.Point(4, 21);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(905, 34);
            this.panel1.TabIndex = 4;
            // 
            // cbItem1Class
            // 
            this.cbItem1Class.FormattingEnabled = true;
            this.cbItem1Class.Items.AddRange(new object[] {
            "食品",
            "五金",
            "冻品",
            "建材",
            "农机",
            "日杂"});
            this.cbItem1Class.Location = new System.Drawing.Point(48, 6);
            this.cbItem1Class.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem1Class.Name = "cbItem1Class";
            this.cbItem1Class.Size = new System.Drawing.Size(55, 25);
            this.cbItem1Class.TabIndex = 0;
            // 
            // nuUDItem1Cnt
            // 
            this.nuUDItem1Cnt.Location = new System.Drawing.Point(720, 6);
            this.nuUDItem1Cnt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nuUDItem1Cnt.Name = "nuUDItem1Cnt";
            this.nuUDItem1Cnt.Size = new System.Drawing.Size(54, 23);
            this.nuUDItem1Cnt.TabIndex = 3;
            // 
            // cbItem1Name
            // 
            this.cbItem1Name.FormattingEnabled = true;
            this.cbItem1Name.Location = new System.Drawing.Point(152, 6);
            this.cbItem1Name.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem1Name.Name = "cbItem1Name";
            this.cbItem1Name.Size = new System.Drawing.Size(89, 25);
            this.cbItem1Name.TabIndex = 0;
            // 
            // tbItem1Freight
            // 
            this.tbItem1Freight.Location = new System.Drawing.Point(832, 6);
            this.tbItem1Freight.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem1Freight.Name = "tbItem1Freight";
            this.tbItem1Freight.Size = new System.Drawing.Size(64, 23);
            this.tbItem1Freight.TabIndex = 2;
            // 
            // cbItem1Pack
            // 
            this.cbItem1Pack.FormattingEnabled = true;
            this.cbItem1Pack.Items.AddRange(new object[] {
            "纸箱",
            "编织袋",
            "泡棉",
            "气泡袋",
            "其他",
            "无"});
            this.cbItem1Pack.Location = new System.Drawing.Point(300, 6);
            this.cbItem1Pack.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbItem1Pack.Name = "cbItem1Pack";
            this.cbItem1Pack.Size = new System.Drawing.Size(55, 25);
            this.cbItem1Pack.TabIndex = 0;
            // 
            // tbItem1Price
            // 
            this.tbItem1Price.Location = new System.Drawing.Point(610, 6);
            this.tbItem1Price.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem1Price.Name = "tbItem1Price";
            this.tbItem1Price.Size = new System.Drawing.Size(57, 23);
            this.tbItem1Price.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 9);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 17);
            this.label17.TabIndex = 1;
            this.label17.Text = "类别：";
            // 
            // tbItem1Wt
            // 
            this.tbItem1Wt.Location = new System.Drawing.Point(508, 6);
            this.tbItem1Wt.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem1Wt.Name = "tbItem1Wt";
            this.tbItem1Wt.Size = new System.Drawing.Size(55, 23);
            this.tbItem1Wt.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(111, 9);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 17);
            this.label16.TabIndex = 1;
            this.label16.Text = "品名：";
            // 
            // tbItem1Vol
            // 
            this.tbItem1Vol.Location = new System.Drawing.Point(404, 6);
            this.tbItem1Vol.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbItem1Vol.Name = "tbItem1Vol";
            this.tbItem1Vol.Size = new System.Drawing.Size(55, 23);
            this.tbItem1Vol.TabIndex = 2;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(256, 9);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 17);
            this.label15.TabIndex = 1;
            this.label15.Text = "包装：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(791, 8);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(44, 17);
            this.label20.TabIndex = 1;
            this.label20.Text = "运费：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(362, 9);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 17);
            this.label14.TabIndex = 1;
            this.label14.Text = "体积：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(570, 9);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(44, 17);
            this.label19.TabIndex = 1;
            this.label19.Text = "单价：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(466, 9);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 17);
            this.label13.TabIndex = 1;
            this.label13.Text = "重量：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(678, 9);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 17);
            this.label18.TabIndex = 1;
            this.label18.Text = "件数：";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox5);
            this.groupBox3.Controls.Add(this.textBox6);
            this.groupBox3.Controls.Add(this.textBox7);
            this.groupBox3.Controls.Add(this.textBox8);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.comboBox3);
            this.groupBox3.ForeColor = System.Drawing.Color.Navy;
            this.groupBox3.Location = new System.Drawing.Point(14, 142);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(914, 55);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "托运人信息";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(692, 24);
            this.textBox5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(211, 23);
            this.textBox5.TabIndex = 2;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(500, 24);
            this.textBox6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(123, 23);
            this.textBox6.TabIndex = 2;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(362, 24);
            this.textBox7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(73, 23);
            this.textBox7.TabIndex = 2;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(188, 24);
            this.textBox8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(104, 23);
            this.textBox8.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(634, 26);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "详细地址：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(458, 26);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 17);
            this.label9.TabIndex = 1;
            this.label9.Text = "招牌：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(320, 26);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 17);
            this.label10.TabIndex = 1;
            this.label10.Text = "姓名：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(146, 26);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 17);
            this.label11.TabIndex = 1;
            this.label11.Text = "手机：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 26);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 17);
            this.label12.TabIndex = 1;
            this.label12.Text = "所在地：";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "都结",
            "杨湾",
            "南圩",
            "隆安",
            "其他"});
            this.comboBox3.Location = new System.Drawing.Point(62, 23);
            this.comboBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(68, 25);
            this.comboBox3.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.ForeColor = System.Drawing.Color.Navy;
            this.groupBox1.Location = new System.Drawing.Point(14, 66);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(914, 55);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "收货人信息";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(693, 23);
            this.textBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(211, 23);
            this.textBox4.TabIndex = 2;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(500, 23);
            this.textBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(123, 23);
            this.textBox3.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(362, 23);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(73, 23);
            this.textBox2.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(189, 23);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(104, 23);
            this.textBox1.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(634, 26);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "详细地址：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(458, 26);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "招牌：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(320, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "姓名：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(147, 26);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "手机：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 26);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "所在地：";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "都结",
            "杨湾",
            "南圩",
            "隆安",
            "其他"});
            this.comboBox1.Location = new System.Drawing.Point(62, 22);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(68, 25);
            this.comboBox1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBox7);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.comboBox2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.dateTimePicker1);
            this.groupBox2.ForeColor = System.Drawing.Color.Navy;
            this.groupBox2.Location = new System.Drawing.Point(14, 5);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(914, 55);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "基本信息";
            // 
            // comboBox7
            // 
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Items.AddRange(new object[] {
            "提付",
            "现付",
            "其他"});
            this.comboBox7.Location = new System.Drawing.Point(500, 21);
            this.comboBox7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(92, 25);
            this.comboBox7.TabIndex = 4;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(437, 24);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 17);
            this.label21.TabIndex = 3;
            this.label21.Text = "支付类型：";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "南宁",
            "隆安",
            "南圩",
            "杨湾",
            "都结",
            "其他"});
            this.comboBox2.Location = new System.Drawing.Point(316, 21);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(92, 25);
            this.comboBox2.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(260, 24);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "收货地：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 24);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "收货日期：";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(79, 22);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(151, 23);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage2.Size = new System.Drawing.Size(936, 512);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "基本信息";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(936, 512);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "考勤记录";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(936, 512);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "相关信息";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(936, 512);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "统计报表";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(936, 512);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "系统维护";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(24, 459);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(68, 33);
            this.button1.TabIndex = 5;
            this.button1.Text = "保存订单";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(105, 459);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(86, 33);
            this.button2.TabIndex = 5;
            this.button2.Text = "打印托运联";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(203, 459);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(86, 33);
            this.button3.TabIndex = 5;
            this.button3.Text = "打印随车联";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(306, 459);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(86, 33);
            this.button4.TabIndex = 5;
            this.button4.Text = "打印收货联";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(422, 459);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(86, 33);
            this.button5.TabIndex = 5;
            this.button5.Text = "一键打印";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(946, 540);
            this.Controls.Add(this.tcCheckIn);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "MainForm";
            this.Text = "LBDClient";
            this.tcCheckIn.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuUDItem5Cnt)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuUDItem4Cnt)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuUDItem3Cnt)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuUDItem2Cnt)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nuUDItem1Cnt)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcCheckIn;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbItem1Wt;
        private System.Windows.Forms.TextBox tbItem1Vol;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cbItem1Class;
        private System.Windows.Forms.TextBox tbItem1Freight;
        private System.Windows.Forms.TextBox tbItem1Price;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown nuUDItem1Cnt;
        private System.Windows.Forms.ComboBox cbItem1Pack;
        private System.Windows.Forms.ComboBox cbItem1Name;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox cbItem5Class;
        private System.Windows.Forms.NumericUpDown nuUDItem5Cnt;
        private System.Windows.Forms.ComboBox cbItem5Name;
        private System.Windows.Forms.TextBox tbItem5Freight;
        private System.Windows.Forms.ComboBox cbItem5Pack;
        private System.Windows.Forms.TextBox tbItem5Price;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox tbItem5Wt;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox tbItem5Vol;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox cbItem4Class;
        private System.Windows.Forms.NumericUpDown nuUDItem4Cnt;
        private System.Windows.Forms.ComboBox cbItem4Name;
        private System.Windows.Forms.TextBox tbItem4Freight;
        private System.Windows.Forms.ComboBox cbItem4Pack;
        private System.Windows.Forms.TextBox tbItem4Price;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbItem4Wt;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbItem4Vol;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cbItem3Class;
        private System.Windows.Forms.NumericUpDown nuUDItem3Cnt;
        private System.Windows.Forms.ComboBox cbItem3Name;
        private System.Windows.Forms.TextBox tbItem3Freight;
        private System.Windows.Forms.ComboBox cbItem3Pack;
        private System.Windows.Forms.TextBox tbItem3Price;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tbItem3Wt;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tbItem3Vol;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbItem2Class;
        private System.Windows.Forms.NumericUpDown nuUDItem2Cnt;
        private System.Windows.Forms.ComboBox cbItem2Name;
        private System.Windows.Forms.TextBox tbItem2Freight;
        private System.Windows.Forms.ComboBox cbItem2Pack;
        private System.Windows.Forms.TextBox tbItem2Price;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbItem2Wt;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox tbItem2Vol;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}

