﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface ICheckInRecordDao
    {
        void createCheckInRecord(CheckInRecord checkInRecord);
        CheckInRecord readCheckInRecord(int checkInRecord);
        void updateCheckInRecord(CheckInRecord checkInRecord);
        void deleteCheckInRecord(int checkInRecord);
    }
}
