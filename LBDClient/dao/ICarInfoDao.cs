﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface ICarInfoDao
    {
        void createCarInfo(CarInfo carInfo);
        CarInfo readCarInfo(int carInfoId);
        void updateCarInfo(CarInfo carInfo);
        void deleteCarInfo(int carInfoId);
    }
}
