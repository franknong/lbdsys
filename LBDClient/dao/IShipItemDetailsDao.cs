﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface IShipItemDetailsDao
    {
        void createShipItemDetails(ShipItemDetails shipItemDetails);
        ShipItemDetails readShipItemDetails(int shipItemDetailsId);
        void updateShipItemDetails(ShipItemDetails shipItemDetails);
        void deleteShipItemDetails(int shipItemDetailsId);
    }
}
