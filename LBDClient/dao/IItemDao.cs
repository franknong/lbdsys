﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface IItemDao
    {
        void createItem(Item item);
        Item readItem(int itemId);
        void updateItem(Item item);
        void deleteItem(int itemId);
    }
}
