﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LBDClient.dao
{
    class DaoFactory
    {
        private static IAddressInfoDao addressInfoDao = new AddressInfoDao();
        private static IBalanceDao balanceDao = new BalanceDao();
        private static ICarInfoDao carInfoDao = new CarInfoDao();
        private static ICheckInRecordDao checkInRecordDao = new CheckInRecordDao();
        private static ICheckOutRecordDao checkOutRecordDao = new CheckOutRecordDao();
        private static IConsigneeInfoDao consigneeInfoDao = new ConsigneeInfoDao();
        private static IShipperInfoDao shipperInfoDao = new ShipperInfoDao();
        private static IShipListDao shipListDao = new ShipListDao();
        private static IShipItemDetailsDao shipItemDetailsDao = new ShipItemDetailsDao();
        private static IShipRecordDao shipRecordDao = new ShipRecordDao();
        private static IEmployeeDao employeeDao = new EmployeeDao();
        private static ITempEmployeeDao tempEmployeeDao = new TempEmployeeDao();
        private static IDistributeRecordDao distributeRecordDao = new DistributeRecordDao();
        private static IOnWayRecordDao onWayRecordDao = new OnWayRecordDao();
        private static IItemDao itemDao = new ItemDao();

        public static IAddressInfoDao getAddressInfoDao()
        {
            return addressInfoDao;
        }
        public static IBalanceDao getBalanceDao()
        {
            return balanceDao;
        }
        public static ICarInfoDao getCarInfoDao()
        {
            return carInfoDao;
        }
        public static ICheckInRecordDao getCheckInRecordDao()
        {
            return checkInRecordDao;
        }
        public static ICheckOutRecordDao getCheckOutRecordDao()
        {
            return checkOutRecordDao;
        }
        public static IConsigneeInfoDao getConsigneeDao()
        {
            return consigneeInfoDao;
        }
        public static IShipperInfoDao getShipperInfoDao()
        {
            return shipperInfoDao;
        }
        public static IShipListDao getShipListDao()
        {
            return shipListDao;
        }
        public static IShipItemDetailsDao getShipItemDetailsDao()
        {
            return shipItemDetailsDao;
        }
        public static IShipRecordDao getShipRecordDao()
        {
            return shipRecordDao;
        }
        public static IEmployeeDao getEmployeeDao()
        {
            return employeeDao;
        }
        public static ITempEmployeeDao getTempEmployeeDao()
        {
            return tempEmployeeDao;
        }
        public static IDistributeRecordDao getDistributeRecordDao()
        {
            return distributeRecordDao;
        }
        public static IItemDao getItemDao()
        {
            return itemDao;
        }
        public static IOnWayRecordDao getOnWayRecordDao()
        {
            return onWayRecordDao;
        }

    }
}
