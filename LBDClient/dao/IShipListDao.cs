﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface IShipListDao
    {
        void  createShipList(ShipList shipList);
        ShipList readShipList(int shipListId);
        void upateShipList(ShipList shipList);
        void deleteShipList(int shipListId);
    }
}
