﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface IConsigneeInfoDao
    {
        void createConsignee(ConsigneeInfo consigneeInfo);
        ConsigneeInfo readConsigneeInfo(int consigneeInfoId);
        void updateConsigneeInfo(ConsigneeInfo consigneeInfo);
        void deleteConsigneeInfo(int consigneeInfoId);
    }
}