﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface IEmployeeDao
    {
        void createEmployee(Employee employee);
        Employee readEmployee(int employeeId);
        void updateEmployee(Employee employee);
        void deleteEmployee(int employeeId);
    }
}
