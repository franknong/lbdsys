﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface IShipRecordDao
    {
        void createShipRecordDao(ShipRecord shipRecord);
        ShipRecord readShipRecord(int shipRecordId);
        void updateShipRecord(ShipRecord shipRecord);
        void deleteShipRecord(int shipRecordId);
    }
}
