﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface IAddressInfoDao
    {
        void createAddressInfo(AddressInfo addressInfo);
        AddressInfo readAddressInfo(int addressInfoId);
        void updateAddressInfo(AddressInfo addressInfo);
        void deleteAddressInfo(int addressInfoId);
    }
}
