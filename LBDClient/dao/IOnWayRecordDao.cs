﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface IOnWayRecordDao
    {
        void createOnWayRecord(OnWayRecord onWayRecord);
        OnWayRecord readOnWayRecord(int onWayRecordId);
        void updateOnWayRecord(int onWayRecordId);
        void deleteOnWayRecord(int onWayRecordId);
    }
}
