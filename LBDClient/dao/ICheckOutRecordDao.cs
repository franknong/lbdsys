﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface ICheckOutRecordDao
    {
        void createCheckOutRecord(CheckOutRecord checkOutRecord);
        CheckOutRecord readCheckOutRecord(int checkOutRecordId);
        void updateCheckOutRecord(CheckOutRecord checkOutRecord);
        void deleteCheckOutRecord(int checkOutRecordId);
    }
}
