﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface IShipperInfoDao
    {
        void createShipperInfo(ShipperInfo shipperInfo);
        ShipperInfo readShipperInfo(int shipperInfoId);
        void updateShipRecord(ShipperInfo shipperInfo);
        void deleteShipperRecord(int shipperInfoId);
    }
}
