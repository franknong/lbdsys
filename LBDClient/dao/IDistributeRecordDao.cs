﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface IDistributeRecordDao
    {
        void createDistributeRecord(DistributeRecord distributeRecord);
        DistributeRecord readDistributeRecord(int distributeRecordId);
        void updateDistributeRecord(DistributeRecord distributeRecord);
        void deleteDistributeRecord(int distributeRecordId);
    }
}
