﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;
namespace LBDClient.dao
{
    interface IBalanceDao
    {
        void createBalance(Balance balance);
        Balance readBalance(int balanceId);
        void upateBalance(Balance balance);
        void deleteBalance(int balanceId);
    }
}
