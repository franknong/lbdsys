﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LBDClient.entity;

namespace LBDClient.dao
{
    interface ITempEmployeeDao
    {
        void createTempEmployee(TempEmployee tempEmployee);
        TempEmployee readTempEmployee(int tempEmployeeId);
        void updateTempEmployee(TempEmployee tempEmployee);
        void deleteTempEmployee(int tempEmployeeId);
    }
}
