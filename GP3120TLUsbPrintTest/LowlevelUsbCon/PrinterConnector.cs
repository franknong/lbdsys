﻿//------------------------------- 

// $Header: /cvsroot/z-bar/msvs/zbar/Zebra.Printing/Connect.cs,v 1.7 2006/11/16 10:55:04 vinorodrigues Exp $ 
using System;
using System.Collections.Generic;
using System.Text;

namespace GP3120TLUsbPrintTest
{

    public abstract class PrinterConnector
    {
        protected abstract void SetConnected(bool value);
        protected abstract bool GetConnected();
        public bool IsConnected
        {
            get { return GetConnected(); }
            set { SetConnected(value); }
        }
        public static readonly int DefaultReadTimeout = 200;
        private int readTimeout = DefaultReadTimeout;
        public int ReadTimeout
        {
            get { return readTimeout; }
            set { readTimeout = value; }
        }
        public static readonly int DefaultWriteTimeout = 200;
        private int writeTimeout = DefaultWriteTimeout;
        public int WriteTimeout
        {
            get { return writeTimeout; }
            set { writeTimeout = value; }
        }
        /* public bool Connect() 
        { 
        SetConnected(true); 
        return GetConnected(); 
        } */
        /* public void Disconnect() 
        { 
        SetConnected(false); 
        } */
        public int Send(byte[] buffer)
        {
            return Send(buffer, 0, buffer.Length);
        }
        public abstract bool BeginSend();
        public abstract void EndSend();
        public abstract int Send(byte[] buffer, int offset, int count);
        public virtual bool CanRead()
        {
            return false;
        }
        /// 
        /// Reads data from the incomming connection. 
        /// 
        /// populated data buffer or null if empty/unsuccessful 
        /// Number of bytes read or -1 if unsuccessful 
        public abstract int Read(byte[] buffer, int offset, int count);
    }
}