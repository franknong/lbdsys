﻿namespace GP3120TLUsbPrintTest
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTraverseUsb = new System.Windows.Forms.Button();
            this.btnLowLevelUsbStream = new System.Windows.Forms.Button();
            this.btnWinspoolPrint = new System.Windows.Forms.Button();
            this.chkbByCmd = new System.Windows.Forms.CheckBox();
            this.chkbUsbEnum = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnTraverseUsb
            // 
            this.btnTraverseUsb.Location = new System.Drawing.Point(12, 12);
            this.btnTraverseUsb.Name = "btnTraverseUsb";
            this.btnTraverseUsb.Size = new System.Drawing.Size(112, 46);
            this.btnTraverseUsb.TabIndex = 0;
            this.btnTraverseUsb.Text = "TraverseUsb";
            this.btnTraverseUsb.UseVisualStyleBackColor = true;
            this.btnTraverseUsb.Click += new System.EventHandler(this.btnTraverseUsb_Click);
            // 
            // btnLowLevelUsbStream
            // 
            this.btnLowLevelUsbStream.Location = new System.Drawing.Point(12, 146);
            this.btnLowLevelUsbStream.Name = "btnLowLevelUsbStream";
            this.btnLowLevelUsbStream.Size = new System.Drawing.Size(112, 46);
            this.btnLowLevelUsbStream.TabIndex = 1;
            this.btnLowLevelUsbStream.Text = "Low Level UsbStream";
            this.btnLowLevelUsbStream.UseVisualStyleBackColor = true;
            this.btnLowLevelUsbStream.Click += new System.EventHandler(this.btnLowLevelUsbStream_Click);
            // 
            // btnWinspoolPrint
            // 
            this.btnWinspoolPrint.Location = new System.Drawing.Point(12, 79);
            this.btnWinspoolPrint.Name = "btnWinspoolPrint";
            this.btnWinspoolPrint.Size = new System.Drawing.Size(112, 46);
            this.btnWinspoolPrint.TabIndex = 1;
            this.btnWinspoolPrint.Text = "Winspool Print";
            this.btnWinspoolPrint.UseVisualStyleBackColor = true;
            this.btnWinspoolPrint.Click += new System.EventHandler(this.btnWinspoolPrint_Click);
            // 
            // chkbByCmd
            // 
            this.chkbByCmd.AutoSize = true;
            this.chkbByCmd.Location = new System.Drawing.Point(155, 94);
            this.chkbByCmd.Name = "chkbByCmd";
            this.chkbByCmd.Size = new System.Drawing.Size(104, 19);
            this.chkbByCmd.TabIndex = 2;
            this.chkbByCmd.Text = "按命令打印";
            this.chkbByCmd.UseVisualStyleBackColor = true;
            // 
            // chkbUsbEnum
            // 
            this.chkbUsbEnum.AutoSize = true;
            this.chkbUsbEnum.Location = new System.Drawing.Point(155, 27);
            this.chkbUsbEnum.Name = "chkbUsbEnum";
            this.chkbUsbEnum.Size = new System.Drawing.Size(85, 19);
            this.chkbUsbEnum.TabIndex = 2;
            this.chkbUsbEnum.Text = "UsbEnum";
            this.chkbUsbEnum.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 369);
            this.Controls.Add(this.chkbUsbEnum);
            this.Controls.Add(this.chkbByCmd);
            this.Controls.Add(this.btnWinspoolPrint);
            this.Controls.Add(this.btnLowLevelUsbStream);
            this.Controls.Add(this.btnTraverseUsb);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTraverseUsb;
        private System.Windows.Forms.Button btnLowLevelUsbStream;
        private System.Windows.Forms.Button btnWinspoolPrint;
        private System.Windows.Forms.CheckBox chkbByCmd;
        private System.Windows.Forms.CheckBox chkbUsbEnum;
    }
}

