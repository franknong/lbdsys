﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace GP3120TLUsbPrintTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// 本方法测试网上下载的关于遍历USB设备的WMDUsbEnum类的遍历功能，以及该分部类中另一部分所实现的输出所有USB设备信息的XML文件的方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTraverseUsb_Click(object sender, EventArgs e)
        {
            if (chkbUsbEnum.Checked)
            {
                UsbHubInfo[] hubInfos=WDKUsbEnum.AllUsbHubs;
                HostControllerInfo[] ctrInfos=WDKUsbEnum.AllHostControllers;
                string hubNames = string.Empty;
                string hubPnpDeviceIds = string.Empty;
                string ctlNames = string.Empty;
                string ctlPnpDeviceIds = string.Empty;
                string hubStatus = string.Empty;
                foreach (UsbHubInfo hub in hubInfos)
                {
                    hubNames+=hub.Name+":";
                    hubPnpDeviceIds+=hub.PNPDeviceID+":";
                    hubStatus += hub.Status + ":";
                }
                MessageBox.Show("hubNames:"+hubNames+"<>hubPnpIds:"+hubPnpDeviceIds+"<>hubStatus:"+hubStatus);

                foreach (HostControllerInfo ctrlInfo in ctrInfos)
                {
                    ctlNames += ctrlInfo.Name + ":";
                    ctlPnpDeviceIds += ctrlInfo.PNPDeviceID + ":";
                }
                MessageBox.Show("ctlNames:" + ctlNames + "<>ctlPnpDeviceIds:" + ctlPnpDeviceIds);
            }
            else
            {
                WDKUsbEnum.EnumUsbToXML("usbs.xml");
            }
        }

        /// <summary>
        /// 本方法测试的是使用地层方法连接USB打印机，然后以流的形式将数据发送到打印机，地层实现中将USB设备当成USB设备文件来操作。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLowLevelUsbStream_Click(object sender, EventArgs e)
        {
            //UsbPrinterStream usbStream = new UsbPrinterStream("Gprinter USB Printer");
            UsbPrinterStream usbStream = new UsbPrinterStream();
            IntPtr thisPtr = this.Handle;
            string cmdStr = "SIZE 60mm,20mm \n"+"CLS \n"+"TEXT 25,25,\"3\",0,1,1,\"FORMFEED COMMAND TEST\" \n"+"PRINT 1,1 \n";
            byte[] buffer = System.Text.Encoding.Default.GetBytes(cmdStr);
            usbStream.Write(buffer, 0, buffer.Length);//其实最终调用的是Kernel32中的WriteFile方法
        }

        private void btnWinspoolPrint_Click(object sender, EventArgs e)
        {
            if (chkbByCmd.Checked)
            {
                //printbyCmd
                string s = "SIZE 60mm,20mm \n" + "CLS \n" + "TEXT 25,25,\"3\",0,1,1,\"FORMFEED COMMAND TEST\" \n" + "PRINT 1,1 \n";// device-dependent string, need a FormFeed?
                PrintDialog pd = new PrintDialog();// Allow the user to select a printer.
                pd.PrinterSettings = new PrinterSettings();
                if (DialogResult.OK == pd.ShowDialog(this))
                {
                    //其实最终调用的是winspool.Drv中的WritePrinter方法
                    RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, s);// Send a printer-specific to the printer.
                }

            }
            else 
            {
                //printByFile
                OpenFileDialog ofd = new OpenFileDialog();// Allow the user to select a file.
                if (DialogResult.OK == ofd.ShowDialog(this))
                {
                    PrintDialog pd = new PrintDialog();// Allow the user to select a printer.
                    pd.PrinterSettings = new PrinterSettings();
                    if (DialogResult.OK == pd.ShowDialog(this))
                    {
                        RawPrinterHelper.SendFileToPrinter(pd.PrinterSettings.PrinterName, ofd.FileName);// Print the file to the printer.
                    }
                }
            }
        }
    }
}
