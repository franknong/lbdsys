﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace WFPrinting
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnPrintTxt_Click(object sender, EventArgs e)
        {
            // Allow the user to select a file.
            OpenFileDialog ofd = new OpenFileDialog();
            if (DialogResult.OK == ofd.ShowDialog(this))
            {
                // Allow the user to select a printer.
                PrintDialog pd = new PrintDialog();
                pd.PrinterSettings = new PrinterSettings();
                if (DialogResult.OK == pd.ShowDialog(this))
                {
                    // Print the file to the printer.
                    RawPrinterHelper.SendFileToPrinter(pd.PrinterSettings.PrinterName, ofd.FileName);
                }
            }
        }

        private void btnPrintCmd_Click(object sender, EventArgs e)
        {
            string s = "SIZE 57mm, 30mm\n BOX 10,10,200,200,5 \n BARCODE 100,100,\"39\",96,1,0,2,4,\"1000\" \n PRINT 1 \n CLS"; // device-dependent string, need a FormFeed?

            // Allow the user to select a printer.
            PrintDialog pd = new PrintDialog();
            pd.PrinterSettings = new PrinterSettings();
            if (DialogResult.OK == pd.ShowDialog(this))
            {
                // Send a printer-specific to the printer.
                RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, "CLS");
                RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, s);
            }
        }

        private void btnTestLib_Click(object sender, EventArgs e)
        {
            int currH = 8;
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "msg", "alert('Hello TSCLIB.DLL')", true);
            //TSCLIB_DLL.about();                                                               //Show the DLL version
            TSCLIB_DLL.openport("GP-3120TL");                                         //Open specified printer driver
            TSCLIB_DLL.setup("60", "96", "4", "8", "0", "0", "0");                              //Setup the media size and sensor type info
            TSCLIB_DLL.clearbuffer();                                                           //Clear image buffer

            TSCLIB_DLL.printerfont("56", currH.ToString(), "TSS24.BF2", "0", "2", "2", "隆保达 NO.");  //Drawing printer font 最宽处456，最高240
            TSCLIB_DLL.barcode("304", currH.ToString(), "128", "56", "1", "0", "2", "2", getItemListNo());
            
            currH = getCurrH(currH, 80, 1, 8);
            TSCLIB_DLL.printerfont("56", currH.ToString(), "TSS24.BF2", "0", "1", "1", "收货人："+getConsigneeInfo());  //Drawing printer font 最宽处456，最高240
            
            currH = getCurrH(currH, 20, 1, 8);
            TSCLIB_DLL.printerfont("56", currH.ToString(), "TSS24.BF2", "0", "1", "1", "托运人："+getShipperInfo());  //Drawing printer font 最宽处456，最高240
            
            string[,] itemTable = getPrintItemsDetail();
            currH = getCurrH(currH, 20, 1, 12);
            Point o = new Point(56, currH);
            printTable(itemTable, o, 32, 72);

            currH = getCurrH(currH, 32, 3, 0);
            TSCLIB_DLL.printerfont("56", currH.ToString(), "TSS24.BF2", "0", "1", "1", "运费合计:"+getFreightSum(itemTable));  //Drawing printer font 最宽处456，最高240
            
            currH = getCurrH(currH, 20, 1, 12);
            TSCLIB_DLL.printerfont("56", currH.ToString(), "TSS24.BF2", "0", "1", "1", "隆保达南宁地址：林招停车场11-1"); //Drawing printer font
            currH = getCurrH(currH, 20, 1, 8);
            TSCLIB_DLL.printerfont("56", currH.ToString(), "TSS24.BF2", "0", "1", "1", "业务：18076557029 店面：13877512789 随车：13877140294"); //Drawing printer font
            currH = getCurrH(currH, 20, 1, 8);
            TSCLIB_DLL.printerfont("56", currH.ToString(), "TSS24.BF2", "0", "1", "1", "QQ：18076557029  网站：www.lbdwl.cn  微信：1205503810"); //Drawing printer font

            currH = getCurrH(currH, 20, 1, 12);
            o.Y = currH;
            o.X = 56;
            int maxW = 56;
            string content = "托运人须知:";
            content += "1. 托运货物必须包装好，若货物不符合包装要求，途中造成货物损坏，后果由托运人负责；";
            content += "2. 严禁夹带危险品、走私品及禁运品，否则一切经济损失和法律责任由托运人负责；";
            content += "3. 托运货物应参加保价运输，贵重物品及易碎品易腐品必须特别声明并参加保价运输，同时注明货物明细；";
            content += "4. 因本部原因造成所托运货物的损坏、丢失、污染，已保价的原价90%赔偿；否则按实际受损程度赔偿，且最高金额不超过该件货物托运费的20倍；";
            content += "5. 如果所托运货物外包装完好，箱内物品数量短少损坏（包括已参加保价运输的），本部不负责赔偿；";
            content += "6. 货物交付托运前，请托运人详细阅读本事项，托运人一经签字，本协议事项即产生法律效力。";
            printNotice(o,maxW,content);

            //TSCLIB_DLL.windowsfont(10, 300, 24, 0, 0, 0, "ARIAL", "Windows Arial Font Test");  //Draw windows font
            //TSCLIB_DLL.downloadpcx("C:\\ASP.NET_in_VCsharp_2008\\ASP.NET_in_VCsharp_2008\\UL.PCX", "UL.PCX");                                         //Download PCX file into printer
            //TSCLIB_DLL.downloadpcx("UL.PCX", "UL.PCX");                                         //Download PCX file into printer
            //TSCLIB_DLL.sendcommand("PUTPCX 100,400,\"UL.PCX\"");                                //Drawing PCX graphic

            TSCLIB_DLL.printlabel("1", "1");                                                    //Print labels
            TSCLIB_DLL.closeport();  
        }

        private string getFreightSum(string[,] itemTable)
        {
            int sum = 0;
            for (int i = 1; i < itemTable.GetLength(0); i++)
            {
                sum+=Convert.ToInt16(itemTable[i,itemTable.GetLength(1)-1]);
            }
            return sum.ToString();
        }

        private string getShipperInfo()
        {
            return "黄生 电话：13565644566";
        }

        private string getConsigneeInfo()
        {
 	        return "都结名流日化 电话：18246515245";
        }

        private void printNotice(Point o, int maxW, string content)
        {
            int fontsPerRow = maxW * 8 / 24;
            int mod = content.Length % fontsPerRow;
            bool flag = (mod == 0);
            int d = content.Length / fontsPerRow;
            int rows = flag?d:d+1;
            int i =0;
            for (; i < rows-1; i++)
            {
                TSCLIB_DLL.printerfont(o.X.ToString(), (o.Y+28 * i).ToString(), "TSS24.BF2", "0", "1", "1", content.Substring(i * fontsPerRow, fontsPerRow));
            }
            if(flag)
                TSCLIB_DLL.printerfont(o.X.ToString(), (o.Y + 28 * i).ToString(), "TSS24.BF2", "0", "1", "1", content.Substring(i * fontsPerRow, fontsPerRow));
            else
                TSCLIB_DLL.printerfont(o.X.ToString(), (o.Y + 28 * i).ToString(), "TSS24.BF2", "0", "1", "1", content.Substring(i * fontsPerRow, mod));
            
        }

        private string getItemListNo()
        {
            return "150410-3";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="beforeH"></param>
        /// <param name="unitH"></param>
        /// <param name="units"></param>
        /// <param name="gapH"></param>
        /// <returns></returns>
        private int getCurrH(int beforeH,int unitH,int units,int gapH)
        {
            return beforeH + unitH * units + gapH;
        }

        private void printTable(string[,] dataTable,Point o,int rowH, int colW)
        {
            int rows = dataTable.GetLength(0);
            int cols = dataTable.GetLength(1);
            for (int rowIdx = 0; rowIdx < rows; rowIdx++)
            {
                for (int colIdx = 0; colIdx < cols; colIdx++)
                {
                    Point p = new Point();
                    p.X = o.X + colW * colIdx;
                    p.Y = o.Y + rowH * rowIdx;
                    TSCLIB_DLL.printerfont(p.X.ToString(), p.Y.ToString(), "TSS24.BF2", "0", "1", "1", dataTable[rowIdx,colIdx]); 
                }
            }
        }

        private string[,] getPrintItemsDetail()
        {
            return new string[,] { 
                {"品名","包装","体积","重量","件数","运费"},
                {"食品","纸箱","4","10","5","20"},
                {"五金","纸箱","0.5","5","4","20"}
            };
        }
        
    }
}
