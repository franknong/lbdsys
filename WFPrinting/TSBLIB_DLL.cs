﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace WFPrinting
{
    public class TSCLIB_DLL
    {
        [DllImport("TSCLIB.dll", EntryPoint = "about")]
        public static extern int about();

        /// <summary>
        /// 说明: 指定计算机端的输出端口
        /// </summary>
        /// <param name="printername">参数:a: 单机打印时，请指定打印机驱动程序名称，例如: TSC TDP-245若连接打印机服务器，请指定服务器路径及共享打印机名称，例如:\\SERVER\ TSC TDP-245</param>
        /// <returns></returns>
        [DllImport("TSCLIB.dll", EntryPoint = "openport")]
        public static extern int openport(string printername);

        /// <summary>
        /// 使用条形码机内建条形码打印
        /// </summary>
        /// <param name="x">条形码X 方向起始点，以点(point)表示</param>
        /// <param name="y">条形码Y 方向起始点，以点(point)表示</param>
        /// <param name="type">"128" Code 128, switching code subset A, B, C automatically;39 Code 39;EAN13 EAN 13;...</param>
        /// <param name="height">设定条形码高度，高度以点来表示</param>
        /// <param name="readable">设定是否打印条形码码文:0: 不打印码文; 1: 打印码文</param>
        /// <param name="rotation">设定条形码旋转角度;0: 旋转0 度;90: 旋转90 度;180: 旋转180 度;270: 旋转270 度</param>
        /// <param name="narrow">窄因子</param>
        /// <param name="wide">宽因子，在手册中有一个表说明不同窄宽比在不同条码类别中的意义</param>
        /// <param name="code">条形码内容</param>
        /// <returns></returns>
        [DllImport("TSCLIB.dll", EntryPoint = "barcode")]
        public static extern int barcode(string x, string y, string type,
                    string height, string readable, string rotation,
                    string narrow, string wide, string code);

        [DllImport("TSCLIB.dll", EntryPoint = "clearbuffer")]
        public static extern int clearbuffer();

        [DllImport("TSCLIB.dll", EntryPoint = "closeport")]
        public static extern int closeport();

        /// <summary>
        /// 下载单色PCX 格式图文件至打印机
        /// </summary>
        /// <param name="filename">文件名(可包含路径)</param>
        /// <param name="image_name">下载至打印机内存内之文件名(请使用大写文件名)</param>
        /// <returns></returns>
        [DllImport("TSCLIB.dll", EntryPoint = "downloadpcx")]
        public static extern int downloadpcx(string filename, string image_name);

        [DllImport("TSCLIB.dll", EntryPoint = "formfeed")]
        public static extern int formfeed();

        [DllImport("TSCLIB.dll", EntryPoint = "nobackfeed")]
        public static extern int nobackfeed();

        /// <summary>
        /// 使用条形码机内建文字打印
        /// </summary>
        /// <param name="x">文字X 方向起始点，以点(point)表示。(200 DPI，1 点=1/8 mm, 300 DPI，1 点=1/12 mm)</param>
        /// <param name="y">文字Y 方向起始点，以点(point)表示。(200 DPI，1 点=1/8 mm, 300 DPI，1 点=1/12 mm)</param>
        /// <param name="fonttype">内建字型名称，共8 种:1: 8*/12点英数字体; 2: 12*20点英数字体; 3: 16*24点英数字体; 4: 24*32点英数字体; 5: 32*48点英数字体; TST24.BF2: 繁体中文24*24; TSS24.BF2: 简体中文24*24; K: 韩文24*24</param>
        /// <param name="rotation">设定文字旋转角度:0: 旋转0 度;90: 旋转90 度;180: 旋转180 度;270: 旋转270 度</param>
        /// <param name="xmul">设定文字X 方向放大倍数，1~8</param>
        /// <param name="ymul">设定文字Y 方向放大倍数，1~8</param>
        /// <param name="text">打印文字内容</param>
        /// <returns></returns>
        [DllImport("TSCLIB.dll", EntryPoint = "printerfont")]
        public static extern int printerfont(string x, string y, string fonttype,
                        string rotation, string xmul, string ymul,
                        string text);

        /// <summary>
        /// 打印标签内容
        /// </summary>
        /// <param name="set">设定打印标签个数(set)</param>
        /// <param name="copy">设定打印标签份数(copy)</param>
        /// <returns></returns>
        [DllImport("TSCLIB.dll", EntryPoint = "printlabel")]
        public static extern int printlabel(string set, string copy);

        /// <summary>
        /// 送内建指令到条形码打印机
        /// </summary>
        /// <param name="printercommand">详细指令请参考编程手册</param>
        /// <returns></returns>
        [DllImport("TSCLIB.dll", EntryPoint = "sendcommand")]
        public static extern int sendcommand(string printercommand);

        /// <summary>
        /// 设定标签的宽度、高度、打印速度、打印浓度、传感器类别、gap/black mark 垂直间距、gap/black mark 偏移距离)
        /// </summary>
        /// <param name="width">设定标签宽度，单位mm</param>
        /// <param name="height">设定标签高度，单位mm</param>
        /// <param name="speed">设定打印速度:"1.0"表示每秒1.0 英寸，以此类推，最大6.0;</param>
        /// <param name="density">设定打印浓度，0~15，数字越大打印结果越黑</param>
        /// <param name="sensor">设定使用传感器类别 0-使用垂直间距传感器(gap sensor)；1-黑标传感器(black mark sensor) </param>
        /// <param name="vertical">设定gap/black mark 垂直间距高度，单位: mm</param>
        /// <param name="offset">设定gap/black mark 偏移距离，单位: mm，此参数若使用一般标签时均设为0</param>
        /// <returns></returns>
        [DllImport("TSCLIB.dll", EntryPoint = "setup")]
        public static extern int setup(string width, string height,
                  string speed, string density,
                  string sensor, string vertical,
                  string offset);

        /// <summary>
        /// 使用Windows TTF 字型打印文字
        /// </summary>
        /// <param name="x">文字X 方向起始点，以点(point)表示。</param>
        /// <param name="y">文字Y 方向起始点，以点(point)表示。</param>
        /// <param name="fontheight">字体高度，以点(point)表示。</param>
        /// <param name="rotation">旋转角度，逆时钟方向旋转：0->0 degree； 90->90 degree；180->180 degree； 270->270 degree</param>
        /// <param name="fontstyle">字体外形0-> 标准(Normal)；1-> 斜体(Italic)；2-> 粗体(Bold)；3-> 粗斜体(Bold and Italic)</param>
        /// <param name="fontunderline">底线；0-> 无底线；1-> 加底线</param>
        /// <param name="szFaceName">字符串型别，字体名称。如: Arial, Times new Roman, 细名体, 标楷体</param>
        /// <param name="content">打印文字内容</param>
        /// <returns></returns>
        [DllImport("TSCLIB.dll", EntryPoint = "windowsfont")]
        public static extern int windowsfont(int x, int y, int fontheight,
                        int rotation, int fontstyle, int fontunderline,
                        string szFaceName, string content);

    }
}
