﻿namespace WFPrinting
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrintTxt = new System.Windows.Forms.Button();
            this.btnPrintCmd = new System.Windows.Forms.Button();
            this.btnTestLib = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPrintTxt
            // 
            this.btnPrintTxt.Location = new System.Drawing.Point(323, 36);
            this.btnPrintTxt.Name = "btnPrintTxt";
            this.btnPrintTxt.Size = new System.Drawing.Size(75, 23);
            this.btnPrintTxt.TabIndex = 0;
            this.btnPrintTxt.Text = "PrintTxt";
            this.btnPrintTxt.UseVisualStyleBackColor = true;
            this.btnPrintTxt.Click += new System.EventHandler(this.btnPrintTxt_Click);
            // 
            // btnPrintCmd
            // 
            this.btnPrintCmd.Location = new System.Drawing.Point(323, 76);
            this.btnPrintCmd.Name = "btnPrintCmd";
            this.btnPrintCmd.Size = new System.Drawing.Size(75, 23);
            this.btnPrintCmd.TabIndex = 1;
            this.btnPrintCmd.Text = "PrintCmd";
            this.btnPrintCmd.UseVisualStyleBackColor = true;
            this.btnPrintCmd.Click += new System.EventHandler(this.btnPrintCmd_Click);
            // 
            // btnTestLib
            // 
            this.btnTestLib.Location = new System.Drawing.Point(323, 117);
            this.btnTestLib.Name = "btnTestLib";
            this.btnTestLib.Size = new System.Drawing.Size(75, 23);
            this.btnTestLib.TabIndex = 2;
            this.btnTestLib.Text = "TestLib";
            this.btnTestLib.UseVisualStyleBackColor = true;
            this.btnTestLib.Click += new System.EventHandler(this.btnTestLib_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 404);
            this.Controls.Add(this.btnTestLib);
            this.Controls.Add(this.btnPrintCmd);
            this.Controls.Add(this.btnPrintTxt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPrintTxt;
        private System.Windows.Forms.Button btnPrintCmd;
        private System.Windows.Forms.Button btnTestLib;
    }
}

